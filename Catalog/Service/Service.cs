﻿using Catalog.Domain;
using Catalog.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalog.Service {
    class Service {
        StudentRepository StudentRepository = new StudentRepository();
        TemaRepository TemaRepository = new TemaRepository();
        NotaRepository NotaRepository = new NotaRepository();

        public Dictionary<string, Student> GetStudents() {
            return StudentRepository.FindAll();
        }

        public Dictionary<string, Tema> GetTeme() {
            return TemaRepository.FindAll();
        }

        public Dictionary<KeyValuePair<string, string>, Nota> GetNote() {
            return NotaRepository.FindAll();
        }

        public Tema AddTema(string id, string description, int deadline, int dateReceied) {
            Tema tema = new Tema(id, description, deadline, dateReceied);

            return TemaRepository.Save(tema);
        }

        public void PrelungireTermen(string id, int newDeadline) {
            Tema tema = TemaRepository.FindOne(id);
            if (tema == null) {
                throw new KeyNotFoundException("Tema not found.");
            }

            TemaRepository.Delete(id);
            tema.Deadline = newDeadline;
            TemaRepository.Save(tema);
        }

        public void AddNota(string idStudent, string idTema, int valoare, string feedback) {
            //if (NotaRepository.FindOne(new KeyValuePair<string, string>(idStudent, idTema)) != null) {
            //    throw new KeyNotFoundException("Nota not found.");
            //}

            Student student = StudentRepository.FindOne(idStudent);

            if (student is null) {
                throw new KeyNotFoundException("Student not found.");
            }

            Tema tema = TemaRepository.FindOne(idTema);

            if (tema is null) {
                throw new KeyNotFoundException("Tema not found.");
            }

            int currentWeek = (DateTime.Today.DayOfYear - 1) / 7 + 1 - 39;

            if (currentWeek > tema.Deadline) {
                valoare = valoare - (currentWeek - tema.Deadline) * 2;
            }

            if (valoare < 1) {
                valoare = 1;
            }

            Nota nota = new Nota(new KeyValuePair<string, string>(idStudent, idTema), student, tema, valoare, currentWeek, feedback);
            NotaRepository.Save(nota);
        }

        public void AddValues(int nrOfValues) {
            Random random = new Random();
            Student Student;
            Tema tema;

            for (int i = 0; i < nrOfValues; i++) {
                Student = new Student(i.ToString(), "Nume Prenume " + i, 220 + i % 8, "email" + i + "@domeniu.com", "Profesor" + i);
                StudentRepository.Save(Student);

                tema = new Tema(i.ToString(), "Descriere tema " + i, i % 12 + 2, i % 12 + 2 - random.Next() % 3);
                TemaRepository.Save(tema);
            }
        }

        public IEnumerable<Student> FiltrareStudentGrupa(int grupa) {
            Dictionary<string, Student> Students = StudentRepository.FindAll();
            
            IEnumerable<Student> TempSt =
                from student in Students
                where student.Value.Grupa == grupa
                select student.Value;
 
            return TempSt; 
        }

        public IEnumerable<Nota> FiltrareNoteTema(string idTema) {
            Dictionary<KeyValuePair<string, string>, Nota> Note = NotaRepository.FindAll();

            IEnumerable<Nota> TempNote =
                from nota in Note
                where nota.Key.Value == idTema
                select nota.Value;

            return TempNote;
        }

        public IEnumerable<Nota> FiltrareNoteStudent(string idStudent) {
            Dictionary<KeyValuePair<string, string>, Nota> Note = NotaRepository.FindAll();

            IEnumerable<Nota> TempNote =
                from nota in Note
                where nota.Key.Key == idStudent
                select nota.Value;

            return TempNote;
        }

        public IEnumerable<Nota> FiltrareNoteGrupaTema(int grupa, string idTema) {
            Dictionary<KeyValuePair<string, string>, Nota> Note = NotaRepository.FindAll();

            IEnumerable<Nota> TempNote =
                from nota in Note
                where nota.Key.Key == idTema && nota.Value.Student.Grupa == grupa
                select nota.Value;

            return TempNote;
        }

        public IEnumerable<Nota> FiltrareNotaData(int begin, int end) {
            Dictionary<KeyValuePair<string, string>, Nota> Note = NotaRepository.FindAll();

            IEnumerable<Nota> TempNote =
                from nota in Note
                where nota.Value.Data > begin && nota.Value.Data < end
                select nota.Value;

            return TempNote;
        }

        public IEnumerable<KeyValuePair<string, double>> raportMedieStudenti() {
            Dictionary<KeyValuePair<string, string>, Nota> Note = NotaRepository.FindAll();

            var TempNote =
                from nota in Note
                group nota.Value.Valoare by nota.Value.Student.Nume into g
                select new KeyValuePair<string,double>(g.Key, g.ToList().Average());
                


            return TempNote;
        }
        
        public KeyValuePair<string, double> RaportTemaMedieMinima() {
            Dictionary<KeyValuePair<string, string>, Nota> Note = NotaRepository.FindAll();

            var TempNote =
                from nota in Note
                group nota.Value.Valoare by nota.Key.Value into g
                orderby g.ToList().Average()
                select new KeyValuePair<string, double>(g.Key, g.ToList().Average());



            return TempNote.First();
        }

        public IEnumerable<KeyValuePair<string, double>> StudentiPermisiExamen() {
            Dictionary<KeyValuePair<string, string>, Nota> Note = NotaRepository.FindAll();

            var TempNote =
                from nota in Note
                group nota.Value.Valoare by nota.Value.Student.Nume into g
                where g.ToList().Average() >= 4
                select new KeyValuePair<string, double>(g.Key, g.ToList().Average());



            return TempNote;
        }

        public IEnumerable<KeyValuePair<string, Student>> RaportStudentiNedepunctati() {
            Dictionary<KeyValuePair<string, string>, Nota> Note = NotaRepository.FindAll();
            Dictionary<string, Student> Students = new Dictionary<string, Student>();
            Student tempStudent;

            IEnumerable<KeyValuePair<string, Student>> StudentsWithGrades =
                from nota in Note
                select new KeyValuePair<string, Student>(nota.Value.Student.Id, nota.Value.Student);

            IEnumerable<string> StudentsNotAllowed =
                from nota in Note
                where nota.Value.Data > nota.Value.Tema.Deadline
                select nota.Key.Key;



            foreach (var student in StudentsWithGrades) {
                if (!(Students.TryGetValue(student.Key, out tempStudent))) {
                    Students.Add(student.Key, student.Value);
                }
                
            }

            foreach(var student in StudentsNotAllowed) {
                Students.Remove(student);
            }


            return Students;
        }



    }
    
}
