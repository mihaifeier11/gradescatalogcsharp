﻿using Catalog.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalog.Repository {
    class NotaRepository : CrudRepository<KeyValuePair<string, string>, Nota> {
        Dictionary<KeyValuePair<string, string>, Nota> Note = new Dictionary<KeyValuePair<string, string>, Nota>();
        string path = @"D:\facultate\Anul 2 Sem 1\MAP\C#\Catalog - v1.1\Catalog\Repository\note.txt";
        StudentRepository StudentRepository = new StudentRepository();
        TemaRepository TemaRepository = new TemaRepository();

        public Nota FindOne(KeyValuePair<string, string> id) {
            if (id.Key is null || id.Value is null) {
                throw new ArgumentException("Id can't be null.");
            }

            StreamReader streamReader = new StreamReader(path);
            string line;

            while ((line = streamReader.ReadLine()) != null) {
                string[] temp = line.Split(',');
                KeyValuePair<string, string> idNota = new KeyValuePair<string, string>(temp[0], temp[1]);
                if (idNota.Equals(id)) {
                    streamReader.Dispose();
                    return new Nota(idNota, StudentRepository.FindOne(idNota.Key), TemaRepository.FindOne(idNota.Value), Int32.Parse(temp[3]), Int32.Parse(temp[3]), temp[4]);
                }
                streamReader.ReadLine();
            }
            streamReader.Dispose();
            return null;
        }

        public Dictionary<KeyValuePair<string, string>, Nota> FindAll() {
            Dictionary<KeyValuePair<string, string>, Nota> Note = new Dictionary<KeyValuePair<string, string>, Nota>();
            StreamReader streamReader = File.OpenText(path);
            Nota nota;
            string line;

            while ((line = streamReader.ReadLine()) != null) {
                string[] temp = line.Split(',');
                KeyValuePair<string, string> idNota = new KeyValuePair<string, string>(temp[0], temp[1]);
                Student student = StudentRepository.FindOne(idNota.Key);
                Tema tema = TemaRepository.FindOne(idNota.Value);
                nota = new Nota(idNota, student, tema, Int32.Parse(temp[2]), Int32.Parse(temp[3]), temp[4]);

                Note.Add(idNota, nota);
                streamReader.ReadLine();
            }
            streamReader.Dispose();
            return Note;
        }

        public Nota Save(Nota entity) {
            if (entity is null) {
                throw new ArgumentException("Entity can't be null.");
            }
            if (FindOne(entity.Id) != null) {
                return entity;
            }
            StreamWriter streamWriter = File.AppendText(path);
            streamWriter.WriteLine(entity.ToString());

            streamWriter.Dispose();
            return null;
        }

        public Nota Delete(KeyValuePair<string, string> id) {
            if (id.Key is null || id.Value is null) {
                throw new ArgumentException("Id can't be null.");
            }

            Dictionary<KeyValuePair<string, string>, Nota> Note = FindAll();
            System.IO.File.WriteAllText(path, string.Empty);

            if (Note.ContainsKey(id)) {
                Nota nota = Note[id];

                Note.Remove(id);

                foreach (KeyValuePair<KeyValuePair<string, string>, Nota> n in Note) {
                    Save(n.Value);
                }

                return nota;
            }
            return null;
        }
 
    }
}
