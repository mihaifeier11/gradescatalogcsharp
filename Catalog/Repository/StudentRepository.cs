﻿using Catalog.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalog.Repository {
    class StudentRepository : CrudRepository<string, Student> {
        string path = @"D:\facultate\Anul 2 Sem 1\MAP\C#\Catalog - v1.1\Catalog\Repository\students.txt";

        public Student FindOne(string id) {
            StreamReader streamReader = new StreamReader(path);
            string line;

            if (id is null) {
                throw new ArgumentException("Id can't be null.");
            }

            while ((line = streamReader.ReadLine()) != null) {
                string[] temp = line.Split(',');

                if (temp[0].Equals(id)) {
                    streamReader.Dispose();
                    return new Student(temp[0], temp[1], Int32.Parse(temp[2]), temp[3], temp[4]);
                }
            }
            streamReader.Dispose();
            return null;
        }

        public Dictionary<String, Student> FindAll() {
            Dictionary<String, Student> Students = new Dictionary<String, Student>();
            StreamReader streamReader = File.OpenText(path);
            Student student;
            string line;
            
            while ((line = streamReader.ReadLine()) != null) {
                string[] temp = line.Split(',');
                student = new Student(temp[0], temp[1], Int32.Parse(temp[2]), temp[3], temp[4]);
                Students.Add(temp[0], student);
                streamReader.ReadLine();
            }
            streamReader.Dispose();
            return Students;
        }

        public Student Save(Student entity) {
            if (entity is null) {
                throw new ArgumentException("Entity can't be null.");
            }
            if (FindOne(entity.Id) != null) {
                return entity;
            }
            StreamWriter streamWriter = File.AppendText(path);
            streamWriter.WriteLine(entity.ToString());
            
            streamWriter.Dispose();
            return null;
        }

        public Student Delete(string id) {
            if (id is null) {
                throw new ArgumentException("Id can't be null.");
            }

            Dictionary<string, Student> Students = FindAll();
            System.IO.File.WriteAllText(path, string.Empty);
            
            if (Students.ContainsKey(id)) {
                Student student = Students[id];

                Students.Remove(id);

                foreach (KeyValuePair<string, Student> st in Students) {
                    Save(st.Value);
                }
                
                return student;
            }



            return null;
        }
    }
}
