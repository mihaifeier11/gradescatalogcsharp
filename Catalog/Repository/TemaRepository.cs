﻿using Catalog.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalog.Repository {
    class TemaRepository : CrudRepository<string, Tema> {
        Dictionary<String, Tema> Teme = new Dictionary<string, Tema>();
        string path = @"D:\facultate\Anul 2 Sem 1\MAP\C#\Catalog - v1.1\Catalog\Repository\teme.txt";

        public Tema FindOne(string id) {
            StreamReader streamReader = new StreamReader(path);
            string line;

            if (id is null) {
                throw new ArgumentException("Id can't be null.");
            }

            while ((line = streamReader.ReadLine()) != null) {
                string[] temp = line.Split(',');

                if (temp[0].Equals(id)) {
                    streamReader.Dispose();
                    return new Tema(temp[0], temp[1], Int32.Parse(temp[2]), Int32.Parse(temp[3]));
                }
            }
            streamReader.Dispose();
            return null;
        }

        public Dictionary<string, Tema> FindAll() {
            Dictionary<String, Tema> Teme = new Dictionary<String, Tema>();
            StreamReader streamReader = File.OpenText(path);
            Tema tema;
            string line;

            while ((line = streamReader.ReadLine()) != null) {
                string[] temp = line.Split(',');
                tema = new Tema(temp[0], temp[1], Int32.Parse(temp[2]), Int32.Parse(temp[3]));
                Teme.Add(temp[0], tema);
                streamReader.ReadLine();
            }
            streamReader.Dispose();
            return Teme;
        }

        public Tema Save(Tema entity) {
            if (entity is null) {
                throw new ArgumentException("Entity can't be null.");
            }

            if (FindOne(entity.Id) != null) {
                return entity;
            }

            StreamWriter streamWriter = File.AppendText(path);
            streamWriter.WriteLine(entity.ToString());
            streamWriter.Dispose();

            return null;
        }

        public Tema Delete(string id) {
            if (id is null) {
                throw new ArgumentException("Id can't be null.");
            }

            Dictionary<string, Tema> Teme = FindAll();
            System.IO.File.WriteAllText(path, string.Empty);

            if (Teme.ContainsKey(id)) {
                Tema tema = Teme[id];

                Teme.Remove(id);

                foreach (KeyValuePair<string, Tema> t in Teme) {
                    Save(t.Value);
                }

                return tema;
            }



            return null;
        }
    }
}
