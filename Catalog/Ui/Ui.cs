﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalog.Domain;
using Catalog.Service;

namespace Catalog.Ui {
    class Ui {
        Service.Service Service = new Service.Service();

        public void Menu() {
            Console.WriteLine("1. Arata toti studentii.");
            Console.WriteLine("2. Arata toate temele.");
            Console.WriteLine("3. Arata toate notele.");
            Console.WriteLine("4. Adauga o tema de laborator.");
            Console.WriteLine("5. Modifica deadline-ul unei teme de laborator.");
            Console.WriteLine("6. Adauga o nota.");
            Console.WriteLine("7. Filtrare grupa.");
            Console.WriteLine("8. Filtrare note la o tema.");
            Console.WriteLine("9. Filtrare notele unui student");
            Console.WriteLine("10. Filtrare notele studentilor dintr-o grupa, la o tema");
            Console.WriteLine("11. Filtrare note dintr-o perioada calendaristica");
            Console.WriteLine("12. Nota la laborator pentru fiecare student.");
            Console.WriteLine("13. Cea mai grea tema.");
            Console.WriteLine("14. Studentii care pot intra in examen.");
            Console.WriteLine("15. Studentii care au predat la timp toate temele.");
            Console.WriteLine("0. Exit.");
        }

        public void ShowAllStudents() {
            foreach(KeyValuePair<string, Student> st in Service.GetStudents()) {
                Console.Write(st.Value.ToString());
            }
        }

        public void ShowAllTeme() {
            foreach (KeyValuePair<string, Tema> t in Service.GetTeme()) {
                Console.Write(t.Value.ToString());
            }
        }

        public void ShowAllNote() {
            foreach (KeyValuePair<KeyValuePair<string, string>, Nota> n in Service.GetNote()) {
                Console.Write(n.Value.ToString());
            }
        }

        public void AddTema() {
            Console.Write("Id: ");
            string id = Console.ReadLine();

            Console.Write("Descriere: ");
            string descriere = Console.ReadLine();

            Console.Write("Deadline: ");
            int deadline = Int32.Parse(Console.ReadLine());

            Console.Write("Data primire: ");
            int dateReceived = Int32.Parse(Console.ReadLine());

            Service.AddTema(id, descriere, deadline, dateReceived);
        }

        public void ModifyDeadline() {
            Console.Write("Id tema: ");
            string id = Console.ReadLine();

            Console.Write("Noul deadline: ");
            int deadline = Int32.Parse(Console.ReadLine());

            Service.PrelungireTermen(id, deadline);
        }

        public void AddNota() {
            Console.Write("Id student: ");
            string idStudent = Console.ReadLine();

            Console.Write("Id tema: ");
            string idTema = Console.ReadLine();

            Console.Write("Nota: ");
            int nota = Int32.Parse(Console.ReadLine());

            Console.Write("Feedback: ");
            string feedback = Console.ReadLine();

            Service.AddNota(idStudent, idTema, nota, feedback);
        }

        public void FiltrareGrupa() {
            Console.Write("Grupa: ");
            int grupa = Int32.Parse(Console.ReadLine());
            
            foreach (var st in Service.FiltrareStudentGrupa(grupa)) {
                Console.Write(st.ToString());
            }
        }

        public void FiltrareNoteTema() {
            Console.Write("Id tema: ");
            string idTema = Console.ReadLine();

            foreach (var nota in Service.FiltrareNoteTema(idTema)) {
                Console.Write(nota.ToString());
            }
        }

        public void FiltrareNoteStudent() {
            Console.Write("Id student: ");
            string idStudent = Console.ReadLine();

            foreach (var nota in Service.FiltrareNoteStudent(idStudent)) {
                Console.Write(nota.ToString());
            }
        }

        public void FiltrareNoteGrupaTema() {
            Console.Write("Grupa: ");
            int grupa = Int32.Parse(Console.ReadLine());

            Console.Write("Id tema: ");
            string idTema = Console.ReadLine();

            foreach (var nota in Service.FiltrareNoteGrupaTema(grupa, idTema)) {
                Console.Write(nota.ToString());
            }
        }

        public void FiltrareNoteData() {
            Console.Write("Data inceput: ");
            int begin = Int32.Parse(Console.ReadLine());

            Console.Write("Data sfarsit: ");
            int end = Int32.Parse(Console.ReadLine());

            foreach (var nota in Service.FiltrareNotaData(begin, end)) {
                Console.Write(nota.ToString());
            }
        }

        public void MedieLaboratorStudent() {
            foreach (var average in Service.raportMedieStudenti()) {
                Console.WriteLine(average.Key + "," + average.Value); ;
                 
            }
        }

        public void RaportNotaMinima() {
            KeyValuePair<string, double> temaNotaMinima = Service.RaportTemaMedieMinima();
            Console.WriteLine(temaNotaMinima.Key + "," + temaNotaMinima.Value);
        }

        public void RaportStudentiPermisiExamen() {
            foreach (var average in Service.StudentiPermisiExamen()) {
                Console.WriteLine(average.Key + "," + average.Value); ;
            }
        }

        public void RaportStudentiNedepunctati() {
            foreach (var average in Service.RaportStudentiNedepunctati()) {
                Console.WriteLine(average.Value.Nume); ;
            }
        }


        public void Start() {
            Service.AddValues(20);
            Menu();
            while (true) {
                Console.Write("Comanda: ");
                string comanda = Console.ReadLine();
                switch (comanda) {
                    case "1":
                        ShowAllStudents();
                        break;

                    case "2":
                        ShowAllTeme();
                        break;

                    case "3":
                        ShowAllNote();
                        break;

                    case "4":
                        AddTema();
                        break;

                    case "5":
                        ModifyDeadline();
                        break;

                    case "6":
                        AddNota();
                        break;

                    case "7":
                        FiltrareGrupa();
                        break;

                    case "8":
                        FiltrareNoteTema();
                        break;

                    case "9":
                        FiltrareNoteStudent();
                        break;

                    case "10":
                        FiltrareNoteGrupaTema();
                        break;

                    case "11":
                        FiltrareNoteData();
                        break;

                    case "12":
                        MedieLaboratorStudent();
                        break;

                    case "13":
                        RaportNotaMinima();
                        break;

                    case "14":
                        RaportStudentiPermisiExamen();
                        break;

                    case "15":
                        RaportStudentiNedepunctati();
                        break;

                    case "0":
                        return;

                    default:
                        Console.WriteLine("Comanda invalida.");
                        break;
                }


            }

        }
    }
}