﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalog.Domain {
    class Average {
        public string IdStudent { get; set; }
        public double AverageGrade { get; set; }

        public Average(string idStudent, double average) {
            this.IdStudent = idStudent;
            this.AverageGrade = average;
        }

        public override string ToString() {
            return IdStudent + "," + AverageGrade + "\n";
        }
    }
}
